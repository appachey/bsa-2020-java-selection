package com.binary_studio.uniq_in_sorted_stream;

import java.util.Comparator;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Stream;

public final class UniqueSortedStream {

	private UniqueSortedStream() {
	}

	public static <T> Predicate<T> distinctById(Function<? super T, ?> idExtractor) {

		Map<Object, Boolean> seen = new ConcurrentHashMap<>();
		return t -> seen.putIfAbsent(idExtractor.apply(t), Boolean.TRUE) == null;
	}

	public static <T> Stream<Row<T>> uniqueRowsSortedByPK(Stream<Row<T>> stream) {
		return stream.filter(distinctById(Row::getPrimaryId)).sorted(Comparator.comparingLong(Row::getPrimaryId));
	}

}
