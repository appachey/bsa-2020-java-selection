package com.binary_studio.fleet_commander.core.ship;

import java.util.Optional;

import com.binary_studio.fleet_commander.core.actions.attack.AttackAction;
import com.binary_studio.fleet_commander.core.actions.defence.AttackResult;
import com.binary_studio.fleet_commander.core.actions.defence.RegenerateAction;
import com.binary_studio.fleet_commander.core.common.Attackable;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.ship.contract.CombatReadyVessel;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

public final class CombatReadyShip implements CombatReadyVessel {

	private String name;

	private PositiveInteger shieldHP;

	private final PositiveInteger shieldHPFull;

	private PositiveInteger hullHP;

	private final PositiveInteger hullHPFull;

	private PositiveInteger powergridOutput;

	private PositiveInteger capacitorAmount;

	private PositiveInteger capacitorRachargeRate;

	private PositiveInteger speed;

	private PositiveInteger size;

	private AttackSubsystem attackSubsystem;

	private DefenciveSubsystem defenciveSubsystem;

	private final PositiveInteger fullCapacitor;

	public CombatReadyShip(String name, PositiveInteger shieldHP, PositiveInteger hullHP,
			PositiveInteger powergridOutput, PositiveInteger capacitorAmount, PositiveInteger capacitorRachargeRate,
			PositiveInteger speed, PositiveInteger size, AttackSubsystem attackSubsystem,
			DefenciveSubsystem defenciveSubsystem) {
		this.name = name;
		this.shieldHP = shieldHP;
		this.shieldHPFull = shieldHP;
		this.hullHP = hullHP;
		this.hullHPFull = hullHP;
		this.powergridOutput = powergridOutput;
		this.capacitorAmount = capacitorAmount;
		this.fullCapacitor = capacitorAmount;
		this.capacitorRachargeRate = capacitorRachargeRate;
		this.speed = speed;
		this.size = size;
		this.attackSubsystem = attackSubsystem;
		this.defenciveSubsystem = defenciveSubsystem;
	}

	@Override
	public void endTurn() {
		this.capacitorAmount = PositiveInteger.of(this.capacitorAmount.value() + this.capacitorRachargeRate.value());
		if (this.capacitorAmount.value() > this.fullCapacitor.value()) {
			this.capacitorAmount = this.fullCapacitor;
		}
	}

	@Override
	public void startTurn() {

	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public PositiveInteger getSize() {
		return this.size;
	}

	@Override
	public PositiveInteger getCurrentSpeed() {
		return this.speed;
	}

	@Override
	public Optional<AttackAction> attack(Attackable target) {
		if (this.capacitorAmount.value() >= this.attackSubsystem.getCapacitorConsumption().value()) {
			this.capacitorAmount = PositiveInteger
					.of(this.capacitorAmount.value() - this.attackSubsystem.getCapacitorConsumption().value());
			return Optional
					.of(new AttackAction(this.attackSubsystem.attack(target), this, target, this.attackSubsystem));
		}
		else {
			return Optional.empty();
		}
	}

	@Override
	public AttackResult applyAttack(AttackAction attack) {
		AttackAction reducedAttack = this.defenciveSubsystem.reduceDamage(attack);
		if (this.shieldHP.value() > 0) {
			int shield = this.shieldHP.value() - reducedAttack.damage.value();
			this.shieldHP = shield < 0 ? PositiveInteger.of(0) : PositiveInteger.of(shield);
		}
		else {
			int hull = this.hullHP.value() - reducedAttack.damage.value();
			this.hullHP = hull < 0 ? PositiveInteger.of(0) : PositiveInteger.of(hull);
		}
		if (this.shieldHP.value() == 0 && this.hullHP.value() == 0) {
			return new AttackResult.Destroyed();
		}
		return new AttackResult.DamageRecived(attack.weapon, reducedAttack.damage, attack.target);
	}

	@Override
	public Optional<RegenerateAction> regenerate() {
		int shieldRegen;
		int hullRegen;
		RegenerateAction regen = this.defenciveSubsystem.regenerate();
		if (this.capacitorAmount.value() >= this.defenciveSubsystem.getCapacitorConsumption().value()) {
			this.capacitorAmount = PositiveInteger
					.of(this.capacitorAmount.value() - this.defenciveSubsystem.getCapacitorConsumption().value());
			if (this.shieldHPFull.equals(this.shieldHP)) {
				shieldRegen = 0;
			}
			else {
				shieldRegen = regen.shieldHPRegenerated.value();
				this.shieldHP = PositiveInteger.of(this.shieldHP.value() + shieldRegen);
				if (this.shieldHP.value() > this.shieldHPFull.value()) {
					this.shieldHP = this.shieldHPFull;
				}
			}
			if (this.hullHPFull.equals(this.hullHP)) {
				hullRegen = 0;
			}
			else {
				hullRegen = regen.hullHPRegenerated.value();
				this.hullHP = PositiveInteger.of(this.hullHP.value() + hullRegen);
				if (this.hullHP.value() > this.hullHPFull.value()) {
					this.hullHP = this.hullHPFull;
				}
			}
			return Optional.of(new RegenerateAction(PositiveInteger.of(shieldRegen), PositiveInteger.of(hullRegen)));
		}
		else {
			return Optional.empty();
		}
	}

}
