package com.binary_studio.fleet_commander.core.ship;

import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.exceptions.InsufficientPowergridException;
import com.binary_studio.fleet_commander.core.exceptions.NotAllSubsystemsFitted;
import com.binary_studio.fleet_commander.core.ship.contract.ModularVessel;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

public final class DockedShip implements ModularVessel {

	private String name;

	private PositiveInteger shieldHP;

	private PositiveInteger hullHP;

	private PositiveInteger powergridOutput;

	private PositiveInteger capacitorAmount;

	private PositiveInteger capacitorRachargeRate;

	private PositiveInteger speed;

	private PositiveInteger size;

	private AttackSubsystem attackSubsystem = null;

	private DefenciveSubsystem defenciveSubsystem = null;

	private DockedShip(String name, PositiveInteger shieldHP, PositiveInteger hullHP, PositiveInteger powergridOutput,
			PositiveInteger capacitorAmount, PositiveInteger capacitorRachargeRate, PositiveInteger speed,
			PositiveInteger size) {
		this.name = name;
		this.shieldHP = shieldHP;
		this.hullHP = hullHP;
		this.powergridOutput = powergridOutput;
		this.capacitorAmount = capacitorAmount;
		this.capacitorRachargeRate = capacitorRachargeRate;
		this.speed = speed;
		this.size = size;
	}

	public static DockedShip construct(String name, PositiveInteger shieldHP, PositiveInteger hullHP,
			PositiveInteger powergridOutput, PositiveInteger capacitorAmount, PositiveInteger capacitorRechargeRate,
			PositiveInteger speed, PositiveInteger size) {
		return new DockedShip(name, shieldHP, hullHP, powergridOutput, capacitorAmount, capacitorRechargeRate, speed,
				size);
	}

	@Override
	public void fitAttackSubsystem(AttackSubsystem subsystem) throws InsufficientPowergridException {
		if (this.attackSubsystem != null && subsystem == null) {
			this.powergridOutput = PositiveInteger
					.of(this.powergridOutput.value() + this.attackSubsystem.getPowerGridConsumption().value());
			this.attackSubsystem = null;
		}
		else if (this.powergridOutput.value() < subsystem.getPowerGridConsumption().value()) {
			throw new InsufficientPowergridException(
					subsystem.getPowerGridConsumption().value() - this.powergridOutput.value());
		}
		else {
			this.attackSubsystem = subsystem;
			this.powergridOutput = PositiveInteger
					.of(this.powergridOutput.value() - subsystem.getPowerGridConsumption().value());
		}
	}

	@Override
	public void fitDefensiveSubsystem(DefenciveSubsystem subsystem) throws InsufficientPowergridException {
		if (this.defenciveSubsystem != null && subsystem == null) {
			this.powergridOutput = PositiveInteger
					.of(this.powergridOutput.value() + this.defenciveSubsystem.getPowerGridConsumption().value());
			this.defenciveSubsystem = null;
		}
		else if (this.powergridOutput.value() < subsystem.getPowerGridConsumption().value()) {
			throw new InsufficientPowergridException(
					subsystem.getPowerGridConsumption().value() - this.powergridOutput.value());
		}
		else {
			this.defenciveSubsystem = subsystem;
			this.powergridOutput = PositiveInteger
					.of(this.powergridOutput.value() - subsystem.getPowerGridConsumption().value());
		}
	}

	public CombatReadyShip undock() throws NotAllSubsystemsFitted {
		if (this.attackSubsystem == null && this.defenciveSubsystem == null) {
			throw NotAllSubsystemsFitted.bothMissing();
		}
		if (this.attackSubsystem == null) {
			throw NotAllSubsystemsFitted.attackMissing();
		}
		if (this.defenciveSubsystem == null) {
			throw NotAllSubsystemsFitted.defenciveMissing();
		}
		return new CombatReadyShip(this.name, this.shieldHP, this.hullHP, this.powergridOutput, this.capacitorAmount,
				this.capacitorRachargeRate, this.speed, this.size, this.attackSubsystem, this.defenciveSubsystem);
	}

}
