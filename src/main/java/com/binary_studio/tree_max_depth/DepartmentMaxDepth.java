package com.binary_studio.tree_max_depth;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public final class DepartmentMaxDepth {

	private DepartmentMaxDepth() {
	}

	private static Integer calculateMaxDepth(List<Department> subDepartments, int depth) {
		List<Department> nonNullList = subDepartments.stream().filter(Objects::nonNull).collect(Collectors.toList());

		Department temp;

		if (nonNullList.isEmpty()) {
			return depth;
		}
		else if (nonNullList.iterator().hasNext()) {
			temp = nonNullList.iterator().next();
			return calculateMaxDepth(temp.subDepartments, depth + 1);
		}
		else {
			return depth;
		}
	}

	public static Integer calculateMaxDepth(Department rootDepartment) {
		if (rootDepartment == null) {
			return 0;
		}
		else {
			return calculateMaxDepth(rootDepartment.subDepartments, 1);
		}
	}

}
